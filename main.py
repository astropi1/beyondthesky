from pathlib import Path
from logzero import logger, logfile
from picamera import PiCamera
from time import sleep
from datetime import datetime, timedelta
from orbit import ISS

#3 Media Scuola media Margherita Hack Castelnuovo (TE)
#Gruppo Beyond the Sky
#Tutor: Matteo Canzari - INAF - Osservatorio Astronomico d'Abruzzo

#UTIL FUNCTIONS

#function to convert the angle 
def convert(angle):
    sign, degrees, minutes, seconds = angle.signed_dms()
    exif_angle = f'{degrees:.0f}/1,{minutes:.0f}/1,{seconds*10:.0f}/10'
    return sign < 0, exif_angle

#function to capture the image with the coordinate
#it accepts camera and image file path
def capture(camera, image):
    #retrive IIS coordinates
    location = ISS.coordinates()
    #convert the coordinates to be compliant with EXIF standard
    south, exif_latitude = convert(location.latitude)
    west, exif_longitude = convert(location.longitude)

    if south == True :
        GPSlatitudeRef = "S"
    else :
        GPSlatitudeRef = "N"

    if west == True :
        GPSlongitudeRef = "W"
    else :
        GPSlongitudeRef = "E"

    #store coordinates
    camera.exif_tags["GPS.GPSLatitude"] = exif_latitude
    camera.exif_tags["GPS.GPSLatitudeRef"] = GPSlatitudeRef
    camera.exif_tags["GPS.GPSLongitude"] = exif_longitude
    camera.exif_tags["GPS.GPSlongitudeRef"] = GPSlongitudeRef

    #capture the image with the coordinates
    camera.capture(image)

#INITIALIZING VARIABLES

#calculate and store the time when the program will be finished. More or less 3hours
#we considered to stop the program 3 minutes before the end. Considering the startup time
future_date = datetime.now() + timedelta(minutes=177)

#retrieve the actual file path
base_folder = Path(__file__).parent.resolve()
#create a log file
logfile(base_folder/"data/events.log")


#define the sleep tim
sleep_time = 9
#initializing the camera with the maximum resolution
cam = PiCamera()
cam.resolution = (2592, 1944)
#start a counter that calculate the number of interation
contatore = 0


#log the program is starter
logger.info(f"program started at {datetime.now()}")

#PROGRAM

# run the program. It will run until it reaches the time calculated to end
#it takes a picture every 10 seconds and store it in the data folder
while datetime.now() < future_date : 
    try:
        #generate the filename
        name = str(base_folder) + "/data/picture_" + str(contatore) + ".JPG"
        
        #takes the picture
        capture(cam, name)

        #log the information
        logger.info(f"picture {contatore} taken at {datetime.now()} and stored in {name}")

        #increase the counter
        contatore = contatore + 1

        #wait for next interation
        sleep(sleep_time)

    except Exception as e:
        #log Exception
        logger.error(f'{e.__class__.__name__}: {e}')

#log the end of activities
logger.info(f"program ended at {datetime.now()}")